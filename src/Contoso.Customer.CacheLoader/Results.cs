using System;
using System.Collections.Generic;

namespace Contoso.Customer.CacheLoader
{
    public class LoadResult
    {
        public EntityResult Customers { get; set; }
        public EntityResult Metrics { get; set; }

        public LoadResult()
        {
            Customers = new EntityResult();            
            Metrics = new EntityResult();            
        }
    }

    public class EntityResult
    {
        public int Count { get; set; }
        public List<Exception> Errors { get; set; }

        public EntityResult()
        {
            Errors = new List<Exception>();
        }
    }
}