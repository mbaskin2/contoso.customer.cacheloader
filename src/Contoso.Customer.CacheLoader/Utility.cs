using System.IO;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Contoso.Customer.Utility
{
    public static class Extensions
    {
        public static async Task<T> DeserializeAsync<T>(this HttpContent content)
        {
            using (StreamReader rdr = new StreamReader(await content.ReadAsStreamAsync()))
            using (JsonReader reader = new JsonTextReader(rdr))
            {
                JsonSerializer serializer = new JsonSerializer();

                return serializer.Deserialize<T>(reader);
            } 
        }
    }
}