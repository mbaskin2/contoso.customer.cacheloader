﻿using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;

namespace Contoso.Customer.CacheLoader
{
    class Program
    {
        static async Task Main(string[] args)
        {
            using IHost host = CreateHostBuilder(args).Build();

            var config = host.Services.GetService(typeof(IConfiguration)) as IConfiguration;

            await Loader.RunAsync(host.Services);

            await host.StopAsync();
        }

        static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
            .ConfigureAppConfiguration((hostingContext, configuration) =>
                {
                    configuration.Sources.Clear();                    

                    configuration
                        .AddJsonFile("appsettings.json")
                        .Build();
                        
                });
    }
}
