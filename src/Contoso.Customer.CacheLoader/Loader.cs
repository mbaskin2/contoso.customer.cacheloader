using Contoso.Customer.Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Abstractions;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace Contoso.Customer.CacheLoader
{
    public class Loader
    {
        private static IConfiguration _config;
        public static ILogger<Loader> _logger;
        public static async Task RunAsync(IServiceProvider serviceProvider)
        {
            _config = serviceProvider.GetService(typeof(IConfiguration)) as IConfiguration;
            _logger = serviceProvider.GetService(typeof(ILogger<Loader>)) as ILogger<Loader>;

            _logger.LogInformation("Loader process is initialized.");
            
            using(var data = new ContosoCustomerDataLayer(await customerContext.InitializeContext(_config)))
            using(var api = new ContosoCustomerApiWrapper(_config))
            {           
                var results = await LoadAsync(api, data);

                LogResults(results);
            }
        }

        public static async Task<LoadResult> LoadAsync(ICustomerApiWrapper api, IDataLayer data)
        {
            _logger.LogInformation("Beginning Contoso Customer cache loader routine...");

            await ClearCachedData(data);

            var results = new LoadResult();

            var customers = await GetDataAsync<customer>(()=> api.GetCustomers(), results.Customers);
            var invalid = customers.Where(c => !customer.isValid(c));
            results.Customers.Errors.AddRange(invalid.Select(m => new Exception("Invalid customer data was not cached. ID fields must have valid values.")));

            foreach(var c in customers.Except(invalid))
            {
                try
                {
                    await data.AddAsync<customer>(c);
                }
                catch(Exception ex)
                {
                    _logger.LogError(ex, "An unexpected error occurred while adding customer data with ID {0}.", c.id);
                    results.Customers.Errors.Add(ex);
                }
                
                var metrics = await GetDataAsync<metric>(()=> api.GetMetricsByCustomerId(c.id), results.Metrics);
                var valid = metrics.Where(m => metric.isValid(m));

                try
                {
                    await data.AddAsync<metric>(valid);                
                }
                catch(Exception ex)
                {
                    _logger.LogError(ex, "An unexpected error occurred while adding metrics data for customer with ID {0}.", c.id);
                    results.Metrics.Errors.Add(ex);
                }

                results.Customers.Count ++;
                results.Metrics.Errors.AddRange(metrics.Except(valid).Select(m => new Exception("Invalid metrics data was not cached. ID fields must have valid values.")));
                results.Metrics.Count += valid.Count();
            }            

            try
            {
                await data.SaveAsync();
            }
            catch(DbUpdateException dbEx)
            {
                _logger.LogError(dbEx, "An error occurred while saving the customer data to the database.");
            }
            catch(Exception ex)
            {
                _logger.LogError(ex, "An unexpected error occurred.");
            }

            return results;
        }        

        private static async Task<IEnumerable<T>> GetDataAsync<T>(Func<Task<IEnumerable<T>>> getDataAsync, EntityResult result)
        {
            var logLabel = typeof(T).Name;
            IEnumerable<T> data = null;
            try
            {
                data = await getDataAsync();
            }
            catch(HttpRequestException hrEx)
            {
                _logger.LogError(hrEx, "The HTTP call to the {0} endpoint failed.", logLabel);
                result.Errors.Append(hrEx);
            }
            catch(JsonException jsEx)
            {
                _logger.LogError(jsEx, "Unable to parse JSON response from {0} endpoint.", logLabel);
                result.Errors.Append(jsEx);
            }
            catch(Exception ex)
            {
                _logger.LogError(ex, "An unexpected error occurred while retrieving {0} data from endpoint.", logLabel);
                result.Errors.Append(ex);
            }

            return data ?? new List<T>();
        }

        private static async Task ClearCachedData(IDataLayer data)
        {
            try
            {
                _logger.LogInformation("Attempting to clear cached data from database...");

                data.RemoveAll<metric>();
                data.RemoveAll<customer>();

                await data.SaveAsync();

                _logger.LogInformation("Cached data successfully deleted.");
            }
            catch(DbUpdateException dbEx)
            {
                _logger.LogError(dbEx, "An error occurred while attempting to clear the cached customer data.");
            }
            catch(Exception ex)
            {
                _logger.LogError(ex, "An unexpected error occurred.");
            }
        }

        private static void LogResults(LoadResult results)
        {
                _logger.LogInformation("Customer cache loader process is complete.");
                _logger.LogInformation("Customer Count: {count}", results.Customers.Count);
                _logger.LogInformation("Customer Error Count: {count}", results.Customers.Errors.Count());
                _logger.LogInformation("Metrics Count: {count}", results.Metrics.Count);
                _logger.LogInformation("Metrics Error Count: {count}", results.Metrics.Errors.Count());
        }
    }
}
