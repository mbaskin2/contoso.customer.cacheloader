using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Contoso.Customer.Utility;
using Contoso.Customer.Data.Entities;
using Microsoft.Extensions.Configuration;

namespace Contoso.Customer.CacheLoader
{
    public interface ICustomerApiWrapper : IDisposable
    {
        Task<IEnumerable<customer>> GetCustomers();
        Task<IEnumerable<metric>> GetMetricsByCustomerId(int id);
    }

    public class ContosoCustomerApiWrapper : ICustomerApiWrapper
    {
        private string _customerEndpoint;
        private string _metricsEndpoint;
        private HttpClient _http;

        public ContosoCustomerApiWrapper(IConfiguration config)
        {
            _http = GetHttpClient(config);
            _customerEndpoint = config.GetValue<string>("ContosoApi:CustomerEndpoint");
            _metricsEndpoint = config.GetValue<string>("ContosoApi:MetricsEndpoint");
        }

        public async Task<IEnumerable<customer>> GetCustomers()
        {
            var response = await _http.GetAsync(_customerEndpoint);
            response.EnsureSuccessStatusCode();
            return await response.Content.DeserializeAsync<IEnumerable<customer>>();
        }

        public async Task<IEnumerable<metric>> GetMetricsByCustomerId(int id)
        {
            var response = await _http.GetAsync(string.Format(_metricsEndpoint, id));
            response.EnsureSuccessStatusCode();
            return await response.Content.DeserializeAsync<IEnumerable<metric>>();
        }

        private static HttpClient GetHttpClient(IConfiguration config)
            => new HttpClient()
            {
                BaseAddress = new Uri(config.GetValue<string>("ContosoApi:BaseAddress")),
                Timeout = new TimeSpan(0,0, 10)
            };

        public void Dispose()
        {
            _http.Dispose();
        }
    }
}
