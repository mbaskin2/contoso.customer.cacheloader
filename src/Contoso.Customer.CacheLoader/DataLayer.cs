using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using Contoso.Customer.Data.Entities;
using Microsoft.EntityFrameworkCore;

namespace Contoso.Customer.CacheLoader
{
    public interface IDataLayer : IDisposable
    {        
        Task AddAsync<T>(T entity) where T : class;
        Task AddAsync<T>(IEnumerable<T> entities) where T : class;
        public void RemoveAll<T>() where T : class;
        Task SaveAsync();
    }

    public class ContosoCustomerDataLayer : IDataLayer
    {
        private customerContext _context;

        public ContosoCustomerDataLayer(customerContext context)
        {
            _context = context;
        }

        public async Task AddAsync<T>(T entity) where T : class
        {
            await _context.Set<T>().AddAsync(entity);
        }

        public async Task AddAsync<T>(IEnumerable<T> entities) where T : class
        {
            await _context.Set<T>().AddRangeAsync(entities);
        }

        public async Task SaveAsync()
        {
            await _context.SaveChangesAsync();
        }

        public void RemoveAll<T>() where T : class
        {
            _context.RemoveRange(_context.Set<T>().AsEnumerable());
        }

        public void Dispose()
        {
            _context.Dispose();
        }
    }
}
