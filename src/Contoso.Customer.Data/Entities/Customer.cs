using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace Contoso.Customer.Data.Entities
{
    public class customer
    {
        public int id { get; set; }
        public string name { get; set; }
        public string representative { get; set; }
        public string representative_email { get; set; }
        public string representative_phone { get; set; }

        public IEnumerable<metric> metrics { get; set; }

        public static bool isValid(customer c)
        {
            return c?.id > 0 &&
                !(string.IsNullOrWhiteSpace(c?.name) || string.IsNullOrWhiteSpace(c?.representative)) &&
                Regex.IsMatch(c?.representative_phone, RegExValidationPatterns.PhoneNumber) &&
                Regex.IsMatch(c?.representative_email, RegExValidationPatterns.EmailAddress);
        }
    }
}
