using System;

namespace Contoso.Customer.Data.Entities
{
    public class metric
    {
        public int id { get; set; }
        public int customer_id { get; set; }
        public string name { get; set; }
        public string expression { get; set; }

        public customer customer { get; set; }

        public static bool isValid(metric m)
        {
            return m?.id > 0 &&
                m?.customer_id > 0 &&
                !(string.IsNullOrWhiteSpace(m?.name) || string.IsNullOrWhiteSpace(m?.expression));
        }
    }
}
