namespace Contoso.Customer.Data
{
    public static class RegExValidationPatterns
    {
        public const string EmailAddress = @"^\S+@\S+\.\S+$";
        public const string PhoneNumber = @"^\d{3}-\d{3}-\d{4}$";
    }
}