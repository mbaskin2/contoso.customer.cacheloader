﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace Contoso.Customer.Data.Entities
{
    public class customerContext : DbContext
    {
        public DbSet<customer> customers { get; set; }
        public DbSet<metric> metrics { get; set; }

        private string _conn;
        private string _provider;
        public customerContext(string connectionString, string provider){
            _conn = connectionString;
            _provider = provider;
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            switch (_provider)
            {
                case ConfiguredDatabaseProviders.SqlServer:
                    optionsBuilder.UseSqlServer(_conn);
                    break;
                case ConfiguredDatabaseProviders.MySql:
                    optionsBuilder.UseMySQL(_conn);
                    break;
                case ConfiguredDatabaseProviders.Npgsql:
                default:
                    optionsBuilder.UseNpgsql(_conn);
                    break;
            }
        }
        
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<customer>().HasKey(e => e.id);
            modelBuilder.Entity<customer>().HasMany<metric>(e => e.metrics).WithOne(e => e.customer).HasForeignKey(e => e.customer_id);
            modelBuilder.Entity<metric>().HasKey(e => new { e.customer_id, e.id });
            modelBuilder.Entity<metric>().HasOne<customer>(e => e.customer).WithMany(e => e.metrics);
        }

        public static async Task<customerContext> InitializeContext(IConfiguration config) 
        {
            var provider = config.GetValue<string>("CustomerContextDb:Provider");
            var connString = config.GetValue<string>("CustomerContextDb:ConnectionString");
            var shouldOverwriteExisting = config.GetValue<bool>("CustomerContextDb:OverwriteExistingDatabase");

            var ctx = new customerContext(connString, provider);

            if (shouldOverwriteExisting)
            {
                await ctx.Database.EnsureDeletedAsync();
            }

            var result = await ctx.Database.EnsureCreatedAsync();

            return ctx;
        }
    

    }

    public static class ConfiguredDatabaseProviders
    {
        public const string Npgsql = "npgsql"; // Postgres
        public const string SqlServer = "sqlserver"; // MS SQL Server
        public const string MySql = "mysql"; // MySQL
    }
}
