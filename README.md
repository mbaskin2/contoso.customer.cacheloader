# Contoso.Customer.CacheLoader

## Introduction
This project is a .**NET Core 3.1** console application written as a coding exercise. It will call the configured Contoso customers API endpoint and retrieve all customer data, and then for each customer call the configured Contoso customer metrics endpoint to retrieve the stored customer metrics. The data is all cached in the configured database. All existing data is deleted and overwritten with new data from the API. If the target database does not exist, the application will create it. Optionally, you can choose to delete the existing database and let the application re-create the schema.

## Setup
In the **appsettings.json** file the target database is specified with a connection string and database provider name. The application supports providers for three popular databases: PostgreSQL, MySQL, and Microsoft SQL Server. Choose a provider and add your connection string in the `CustomerContextDb` section, as shown here:
```json
"CustomerContextDb": {

	"Provider": "npgsql",
	"ConnectionString": "Host=localhost;Database=customer;Username=postgres;Password=password",
	"OverwriteExistingDatabase": true
},
```
Valid values for `Provider` are:
 - "npgsql"
 - "mysql"
 - "sqlserver"

**NOTE:** Be sure to provide a valid connection string according to the database server you plan to target.

## Output
The application will write all data to a database named `customer` in the target server. The schema will consist of two related tables: `customers` and `metrics`.  The application will log messages to the console in the default configuration, providing details about progress, errors encountered, and a summary at the end of the run displaying counts of the number of customer records cached, the number of metrics records cached, and the number of errors encountered for each.