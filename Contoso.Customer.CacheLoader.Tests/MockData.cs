using System.Collections.Generic;
using System.Linq;
using Contoso.Customer.Data.Entities;

public static class MockData
{
    private static List<customer> _validCustomers;
    private static List<customer> _invalidCustomers;
    private static List<metric> _validMetrics;
    private static List<metric> _invalidMetrics;
    public static IEnumerable<customer> ValidCustomers
    {
        get {
            return _validCustomers;
        }
    }

    public static IEnumerable<customer> AllCustomers
    {
        get {
            return _invalidCustomers.Concat(_validCustomers);
        }
    }

    public static IEnumerable<metric> ValidMetrics
    {
        get {
            return _validMetrics;
        }
    }

    public static IEnumerable<metric> AllMetrics
    {
        get {
            return _invalidMetrics.Concat(_validMetrics);
        }
    }

    static MockData()
    {
        _validCustomers = new List<customer>();
        _validCustomers.AddRange(
            new customer[]
            {
                new customer {
                    id = 1,
                    name = "ABC Corporation",
                    representative = "John Smith",
                    representative_email = "john.smith@abc.com",
                    representative_phone = "555-555-1234"                        
                },
                new customer {
                    id = 2,
                    name = "XYZ Solutions",
                    representative = "Jane Doe",
                    representative_email = "jane.doe@xyz.biz",
                    representative_phone = "555-555-9999"                        
                },
                new customer {
                    id = 3,
                    name = "The Foo Group",
                    representative = "Bar Foo",
                    representative_email = "bar.foo@foo-group.info",
                    representative_phone = "555-555-1111"                        
                }
            }
        );

        _invalidCustomers = new List<customer>();
        _invalidCustomers.AddRange(
            new customer[]
            {
                new customer {
                    id = 0,
                    name = "Invalid ID Corporation",
                    representative = "John Smith",
                    representative_email = "john.smith@abc.com",
                    representative_phone = "555-555-1234"                        
                },
                new customer {
                    id = 4,
                    name = "",
                    representative = "No Name",
                    representative_email = "no.name@xyz.com",
                    representative_phone = "555-555-9999"                        
                },
                new customer {
                    id = 5,
                    name = "Invalid Representative",
                    representative = "",
                    representative_email = "jane.doe@xyz.com",
                    representative_phone = "555-555-9999"                        
                },
                new customer {
                    id = 6,
                    name = "Invalid Email",
                    representative = "Jane Doe",
                    representative_email = "not-a-valid-email",
                    representative_phone = "555-555-9999"                    
                },
                new customer {
                    id = 7,
                    name = "Invalid Phone Number",
                    representative = "Jane Doe",
                    representative_email = "jane.doe@xyz.com",
                    representative_phone = "9-99-999"
                }
            }
        );

        _validMetrics = new List<metric>();
        _validMetrics.AddRange(
            new metric[] {
                new metric {
                    id = 1,
                    customer_id = 1,
                    name = "Some Metric",
                    expression = "some expression"
                },
                new metric {
                    id = 2,
                    customer_id = 1,
                    name = "Some Metric",
                    expression = "some expression"
                },
                new metric {
                    id = 1,
                    customer_id = 2,
                    name = "Some Metric",
                    expression = "some expression"
                },
                new metric {
                    id = 1,
                    customer_id = 3,
                    name = "Some Metric",
                    expression = "some expression"
                }                
            }
        );

        _invalidMetrics = new List<metric>();
        _invalidMetrics.AddRange(
            new metric[] {
                new metric {
                    id = 0,
                    customer_id = 1,
                    name = "No ID Metric",
                    expression = "some expression"
                },
                new metric {
                    id = 3,
                    customer_id = 1,
                    name = "",
                    expression = "no name expression"
                },
                new metric {
                    id = 2,
                    customer_id = 2,
                    name = "No Expression Metric",
                    expression = ""
                },
            }
        );
    }
}