using System;
using System.Linq;
using Xunit;
using Contoso.Customer.CacheLoader;
using System.Threading.Tasks;
using Contoso.Customer.Data.Entities;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Abstractions;

namespace Contoso.Customer.CacheLoader.Tests
{
    public class Loader_LoadAsyncShould
    {
        private MockDataLayer _data;
        private MockApiWrapper _api;

        public Loader_LoadAsyncShould()
        {
            _data = new MockDataLayer();
            _api = new MockApiWrapper();
            Loader._logger = NullLogger<Loader>.Instance;
        }
        
        [Fact(DisplayName = "All valid customer input objects from the API should be added to the data.")]
        public async Task LoadAsyncShould_ImportValidCustomers()
        {
            _api.Customers = MockData.ValidCustomers;

            await Loader.LoadAsync(_api, _data);

            Assert.True(_data.Data<customer>().Count() == MockData.ValidCustomers.Count(), "All valid customers were not imported.");
        }

        [Fact(DisplayName = "All invalid customer input objects from the API should be skipped and counted as errors.")]
        public async Task LoadAsyncShould_SkipInvalidCustomers()
        {
            _api.Customers = MockData.AllCustomers;

            var results = await Loader.LoadAsync(_api, _data);

            Assert.True(_data.Data<customer>().Count == MockData.ValidCustomers.Count(), "All valid customers were not imported.");
            Assert.True(results.Customers.Errors.Count == 3, "Invalid customer errors are not reported in results.");
        }

        [Fact(DisplayName = "All valid metrics input objects from the API should be added to the data.")]
        public async Task LoadAsyncShould_ImportValidMetrics()
        {
            _api.Customers = MockData.ValidCustomers;
            _api.Metrics = MockData.ValidMetrics;

            var results = await Loader.LoadAsync(_api, _data);

            Assert.True(_data.Data<metric>().Count == MockData.ValidMetrics.Count(), "All valid metrics were not imported.");
        }

        [Fact(DisplayName = "All invalid metrics input objects from the API should be skipped and counted as errors.")]
        public async Task LoadAsyncShould_SkipInvalidMetrics()
        {
            _api.Customers = MockData.ValidCustomers;
            _api.Metrics = MockData.AllMetrics;

            var results = await Loader.LoadAsync(_api, _data);

            Assert.True(_data.Data<metric>().Count == MockData.ValidMetrics.Count(), "All valid metrics were not imported.");
            Assert.True(results.Metrics.Errors.Count == 5, "Invalid metrics errors are not reported in results.");
        }
    }
}
