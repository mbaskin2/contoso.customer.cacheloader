using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Contoso.Customer.CacheLoader;

namespace Contoso.Customer.CacheLoader.Tests
{
    public class MockDataLayer : IDataLayer
    {
        private Dictionary<string, List<Object>> _store;

        public List<object> Data<T>()
        {
            return _store.GetValueOrDefault(typeof(T).Name);
        }

        public MockDataLayer()
        {
            _store = new Dictionary<string, List<object>>();
        } 
        public Task AddAsync<T>(T entity) where T : class
        {
            if(_store.ContainsKey(typeof(T).Name))
            {
                _store.GetValueOrDefault(typeof(T).Name)?.Add(entity);
            }
            else
            {
                var list = new List<object>();
                list.Add(entity);
                _store.Add(typeof(T).Name, list);
            }

            return Task.CompletedTask;
        }

        public Task AddAsync<T>(IEnumerable<T> entities) where T : class
        {
            if(_store.ContainsKey(typeof(T).Name))
            {
                _store.GetValueOrDefault(typeof(T).Name)?.AddRange(entities);
            }
            else
            {
                var list = new List<object>();
                list.AddRange(entities);
                _store.Add(typeof(T).Name, list);
            }

            return Task.CompletedTask;
        }

        public void Dispose()
        {
            foreach(var l in _store.Values)
            {
                l.Clear();
            }
            _store.Clear();
        }

        public void RemoveAll<T>() where T : class
        {
            _store.GetValueOrDefault(typeof(T).Name).Clear();
        }

        public Task SaveAsync()
        {
            return Task.CompletedTask;
        }
    }

}
