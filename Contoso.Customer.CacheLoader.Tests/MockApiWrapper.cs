using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using Contoso.Customer.CacheLoader;
using Contoso.Customer.Data.Entities;

namespace Contoso.Customer.CacheLoader.Tests
{
    public class MockApiWrapper : ICustomerApiWrapper
    {
        private IEnumerable<customer> _customers = new List<customer>();
        private IEnumerable<metric> _metrics = new List<metric>();

        public IEnumerable<customer> Customers { get => _customers; set => _customers = value; }
        public IEnumerable<metric> Metrics { get => _metrics; set => _metrics = value; }

        public void Dispose()
        {
            return;
        }

        public Task<IEnumerable<customer>> GetCustomers()
        {
            return Task.FromResult(_customers);
        }

        public Task<IEnumerable<metric>> GetMetricsByCustomerId(int id)
        {
            return Task.FromResult(_metrics.Where(m => m.customer_id == id));
        }
    }
}
